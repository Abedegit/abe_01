# -*- coding:utf-8 -*-
import pytest
from common import context
from common import constant
from common.ExcelTools import DoExcel


pytestmark = pytest.mark.slow    #这个是标记本模块，pytestmark是固定命名，作用与此模块的所有类和函数

@pytest.mark.smoke
@pytest.mark.usefixtures("common_instance")
class TestAuthorization:
    excel = DoExcel(constant.excel_dir, "Authorization")
    cases = excel.read_excel()

    @pytest.mark.parametrize("case", cases)
    def test_get_token(self, common_instance, case):
        cnname = "Rep" + str(context.create_number())
        enname = "Rep" + str(context.create_number())
        tel = context.create_phone()
        sn = context.create_number()
        setattr(context.Context, 'cnName', cnname)
        setattr(context.Context, 'enName', cnname)
        setattr(context.Context, 'tel', str(tel))
        setattr(context.Context, 'sn', sn)
        common_instance[3].mylog.info("当前执行的用例是:{}".format(case.title))

        url = common_instance[0].get_value("dev_info", "oa_domain_names") + case.url
        case_data = context.param_replace(case.data)
        res = common_instance[1].http_request(case.method, url, data=case_data, headers=case.headers)
        if case.case_id == 1:
            setattr(context.Context, "token", res.json()['accessToken'])
            print(res.json()['accessToken'])
            setattr(context.Context, "userId", str(res.json()['userId']))
        if case.case_id == 2:
            setattr(context.Context, "Approver1_token", res.json()['accessToken'])
        if case.case_id == 3:
            setattr(context.Context, "Approver2_token", res.json()['accessToken'])
        if case.case_id == 4:
            setattr(context.Context, "hr_token", res.json()['accessToken'])
        global result
        try:
            assert case.expected == res.status_code
            result = "pass"
        except AssertionError as e:
            result = "fail"
            raise e
        finally:
            # self.excel.write_excel(case.case_id, res.status_code, result)
            common_instance[3].mylog.info("当前执行的结果是:{}".format(result))
