import pytest
from common import constant
from common.HttpRequest import Request
from common.ConfTools import DoConf
from common.LogTools import DoLogs
from common.SqlTools import DoMysql


@pytest.fixture(scope="class")
def common_instance():
    cf = DoConf(constant.globe_conf_dir)
    resp = Request()
    mysql = DoMysql()
    log = DoLogs(__name__)
    yield cf, resp, mysql, log
    resp.close()
    mysql.close()
